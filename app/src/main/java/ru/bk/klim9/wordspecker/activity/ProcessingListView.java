package ru.bk.klim9.wordspecker.activity;

/**
 * Created by Ivan on 02.10.16.
 */
public interface ProcessingListView {

    void showProgress();
    void hideProgress();
    void deleteClick(int position);
}
