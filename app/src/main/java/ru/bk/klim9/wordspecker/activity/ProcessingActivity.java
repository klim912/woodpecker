package ru.bk.klim9.wordspecker.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.bk.klim9.wordspecker.R;
import ru.bk.klim9.wordspecker.presenter.ProcessingPresenter;
import ru.bk.klim9.wordspecker.presenter.ProcessingPresenterImpl;

public class ProcessingActivity extends AppCompatActivity implements ProcessingActivityView {

    @Bind(R.id.paste_text)
    EditText pasteText;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    ProcessingPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processing);

        ButterKnife.bind(this);

        presenter = new ProcessingPresenterImpl(this);

        progressBar.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_processing)
    public void click(){
        String text = pasteText.getText().toString();
        presenter.getWordsList(text);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setTextError() {
        pasteText.setError(getString(R.string.text_error));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void navigateToEditList(ArrayList<String> list) {
        Intent intent = new Intent(this, ProcessingListActivity.class);
        intent.putStringArrayListExtra("list", list);
        startActivity(intent);

    }
}
