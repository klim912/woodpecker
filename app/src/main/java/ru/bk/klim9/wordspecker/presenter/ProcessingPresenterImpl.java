package ru.bk.klim9.wordspecker.presenter;

import java.util.ArrayList;

import ru.bk.klim9.wordspecker.activity.ProcessingActivityView;
import ru.bk.klim9.wordspecker.interactor.ProcessingInteractor;
import ru.bk.klim9.wordspecker.interactor.ProcessingInteractorImpl;

/**
 * Created by Ivan on 21.09.16.
 */
public class ProcessingPresenterImpl implements ProcessingPresenter, ProcessingInteractor.OnProcessingFinishedListener{

    ProcessingActivityView processingView;
    ProcessingInteractor processingInteractor;

    public ProcessingPresenterImpl(ProcessingActivityView view) {
        this.processingView = view;
        processingInteractor = new ProcessingInteractorImpl();
    }

    @Override
    public void onDestroy() {
        processingView = null;
    }

    @Override
    public void getWordsList(String text) {
        if (processingView != null){
            processingView.showProgress();
        }
        processingInteractor.processing(text, this);
    }

    @Override
    public void onTextError() {
        if (processingView != null){
            processingView.setTextError();
            processingView.hideProgress();
        }

    }

    @Override
    public void onSuccess(ArrayList<String> list) {
        processingView.navigateToEditList(list);

    }
}
