package ru.bk.klim9.wordspecker.interactor;

import java.util.ArrayList;

/**
 * Created by Ivan on 28.09.16.
 */
public interface ProcessingInteractor {

    interface OnProcessingFinishedListener {
        void onTextError();
        void onSuccess(ArrayList<String> list);
    }
    void processing(String text, OnProcessingFinishedListener listener);
}
