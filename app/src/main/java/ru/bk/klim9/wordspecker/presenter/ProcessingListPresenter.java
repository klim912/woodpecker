package ru.bk.klim9.wordspecker.presenter;

/**
 * Created by Ivan on 02.10.16.
 */
public interface ProcessingListPresenter {

    void onResume();
    void onDestroy();
}
