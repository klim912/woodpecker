package ru.bk.klim9.wordspecker.presenter;

import ru.bk.klim9.wordspecker.activity.ProcessingListView;
import ru.bk.klim9.wordspecker.interactor.ProcessingListInteractor;
import ru.bk.klim9.wordspecker.interactor.ProcessingListInteractorImpl;

/**
 * Created by Ivan on 02.10.16.
 */
public class ProcessingListPresenterImpl implements ProcessingListPresenter{

    private ProcessingListView listView;
    private ProcessingListInteractor listInteractor;

    public ProcessingListPresenterImpl(ProcessingListView view) {
        this.listView = view;
        this.listInteractor = new ProcessingListInteractorImpl();
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }
}
