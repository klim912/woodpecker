package ru.bk.klim9.wordspecker.activity;

import java.util.ArrayList;

/**
 * Created by Ivan on 21.09.16.
 */
public interface ProcessingActivityView {

    void showProgress();
    void hideProgress();
    void setTextError();
    void navigateToEditList(ArrayList<String> list);
}
