package ru.bk.klim9.wordspecker.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ru.bk.klim9.wordspecker.R;
import ru.bk.klim9.wordspecker.activity.ProcessingListActivity;
import ru.bk.klim9.wordspecker.activity.ProcessingListView;

public class RawListAdapter extends RecyclerView.Adapter<RawListAdapter.MyViewHolder> {

    private LayoutInflater mInflater;
    private List<String> mItemList;
    Context mContext;

    public RawListAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mItemList = data;
        this.mContext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.item_full_list, viewGroup, false);
        MyViewHolder holder = new MyViewHolder(view, mContext);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder viewHolder, int i) {
        String value = mItemList.get(i);

        viewHolder.word.setText(value);

//        Picasso.with(mContext).load(photo.getPicture().getUrl())
//                .into(viewHolder.mImage);

    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

//    @Override
//    public void onViewRecycled(MyViewHolder holder) {
//        holder.cleanup();
//    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView word;
        private ImageView del;
        private Context mContext;

        public MyViewHolder(View itemView, Context context) {
            super(itemView);
            word = (TextView) itemView.findViewById(R.id.word);
            del = (ImageView) itemView.findViewById(R.id.angry_btn);
            mContext = context;
            del.setOnClickListener(this);
        }

//        public void cleanup() {
//            Picasso.with(mImage.getContext())
//                    .cancelRequest(mImage);
//            mImage.setImageDrawable(null);
//        }

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.angry_btn:
                    ProcessingListView listView = (ProcessingListView)mContext;
                    listView.deleteClick(getPosition());
                    break;
            }
        }
    }
}
