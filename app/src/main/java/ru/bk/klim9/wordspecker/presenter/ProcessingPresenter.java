package ru.bk.klim9.wordspecker.presenter;

/**
 * Created by Ivan on 21.09.16.
 */
public interface ProcessingPresenter {
    void getWordsList(String text);
    void onDestroy();
}
