package ru.bk.klim9.wordspecker.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ru.bk.klim9.wordspecker.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button browserButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        browserButton = (Button)findViewById(R.id.button);
        browserButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                startActivity(new Intent(this, ProcessingActivity.class));
                break;
        }
    }
}
