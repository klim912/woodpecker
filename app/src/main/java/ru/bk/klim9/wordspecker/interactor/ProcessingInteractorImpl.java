package ru.bk.klim9.wordspecker.interactor;

import android.os.Handler;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * Created by Ivan on 28.09.16.
 */
public class ProcessingInteractorImpl implements ProcessingInteractor{

    @Override
    public void processing(final String text, final OnProcessingFinishedListener listener) {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                boolean error = false;
                if (TextUtils.isEmpty(text)){
                    listener.onTextError();
                    error = true;
                }
                if (!error){
                    String text1 = text.replaceAll(",", "");
                    String text2 = text1.replaceAll(".", "");
                    String[] array = text1.split(" ");
                    ArrayList<String> list = new ArrayList<String>();
                    for (int i = 0; i < array.length; i++){
                        list.add(array[i]);
                    }
                    listener.onSuccess(list);
                }
            }
        }, 2000);
    }
}
