package ru.bk.klim9.wordspecker.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.bk.klim9.wordspecker.R;
import ru.bk.klim9.wordspecker.adapter.DividerItemDecoration;
import ru.bk.klim9.wordspecker.adapter.RawListAdapter;
import ru.bk.klim9.wordspecker.presenter.ProcessingListPresenter;
import ru.bk.klim9.wordspecker.presenter.ProcessingListPresenterImpl;

public class ProcessingListActivity extends AppCompatActivity implements ProcessingListView {

    @Bind(R.id.progressBar2)
    ProgressBar progressBar;

    @Bind(R.id.rv_raw_list)
    RecyclerView rvRawList;

    private ProcessingListPresenter presenter;
    private ArrayList<String> mItemList;
    private RawListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_processing_list);

        ButterKnife.bind(this);

        mItemList = getIntent().getStringArrayListExtra("list");
        presenter = new ProcessingListPresenterImpl(this);

        adapter = new RawListAdapter(this, mItemList);
        rvRawList.addItemDecoration(new DividerItemDecoration(getResources()));
        rvRawList.setAdapter(adapter);
        rvRawList.setLayoutManager(new LinearLayoutManager(this));
        progressBar.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void deleteClick(int position) {
        mItemList.remove(position);
        adapter.notifyDataSetChanged();
    }
}
